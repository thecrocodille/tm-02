package ru.thecrocodille.taskmanager.entity;

public class Task {
    private int taskID;
    private String taskName;
    private static int counter = 0;

    public Task(String taskName) {
        this.taskName = taskName;
        counter++;
        this.taskID = counter;
    }

    public int getTaskID() {
        return taskID;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
    
    
    
    
}
